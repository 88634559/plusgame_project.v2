package com.example.pluegameproject

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Button
import android.widget.TextView
import kotlin.random.Random

private var countWin = 0
private var countLose = 0
private var sum = 0
private val intent = Intent()
private var num1 = Random.nextInt(0, 11)
private var num2 = Random.nextInt(0, 11)

class GameActivity2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game2)

        StartGame()
    }

    private fun StartGame() {
        sum = 0
        sum = Game()
        intent.putExtra("Win", countWin)
        intent.putExtra("Lose", countLose)
        setResult(Activity.RESULT_OK, intent)
        Check(sum)
        ResultCount()
        //        Handler().postDelayed({
        //            sum = 0
        //        }, 2000)
    }

    private fun Check(sum: Int) {
        val btnSum4 = findViewById<Button>(R.id.btnSum4)
        val btnSum5 = findViewById<Button>(R.id.btnSum5)
        val btnSum6 = findViewById<Button>(R.id.btnSum6)

        val ranBtn = Random.nextInt(1, 3)

        if (ranBtn == 1) {
            btnSum4.text = sum.toString()
            btnSum5.text = (sum + 1).toString()
            btnSum6.text = (sum + 2).toString()
        }
        if (ranBtn == 2) {
            btnSum4.text = (sum - 1).toString()
            btnSum5.text = sum.toString()
            btnSum6.text = (sum + 1).toString()
        }
        if (ranBtn == 3) {
            btnSum4.text = (sum - 2).toString()
            btnSum5.text = (sum - 1).toString()
            btnSum6.text = sum.toString()
        } else {
            btnSum4.setOnClickListener {
                if (btnSum4.text == sum.toString()) {
                    countWin += 1
                    StartGame()
                } else {
                    countLose += 1
                    StartGame()
                }
            }
            btnSum5.setOnClickListener {
                if (btnSum5.text == sum.toString()) {
                    countWin += 1
                    StartGame()
                } else {
                    countLose += 1
                    StartGame()
                }
            }
            btnSum6.setOnClickListener {
                if (btnSum6.text == sum.toString()) {
                    countWin += 1
                    StartGame()
                } else {
                    countLose += 1
                    StartGame()
                }
            }
        }
    }

    private fun ResultCount(): Pair<TextView, TextView> {
        val textCountWin2 = findViewById<TextView>(R.id.textCountWin2)
        val textCountLose2 = findViewById<TextView>(R.id.textCountLose2)
        textCountWin2.text = "Win: "+countWin.toString()
        textCountLose2.text = "Lose: "+countLose.toString()
        return Pair(textCountWin2, textCountLose2)
    }

    private fun Game(): Int {
        val textNum3 = findViewById<TextView>(R.id.textNum3)
        val textNum4 = findViewById<TextView>(R.id.textNum4)
        num1 = Random.nextInt(0, 11)
        num2 = Random.nextInt(0, 11)
        textNum3.text = num1.toString()
        textNum4.text = num2.toString()

        return num1 - num2
    }
}