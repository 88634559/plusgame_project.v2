package com.example.pluegameproject

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.example.pluegameproject.databinding.FragmentGameBinding
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [GameFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class GameFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var countWin = 0
    private var countLose = 0
    private var sum = 0
    private var intent = Intent()
    private var num1 = Random.nextInt(0, 11)
    private var num2 = Random.nextInt(0, 11)
    private lateinit var binding: FragmentGameBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentGameBinding>(inflater,
            R.layout.fragment_game,container,false)
        return binding.root
    }

    private fun StartGame(){
        sum = 0
        sum = Game()
        intent.putExtra("Win", countWin)
        intent.putExtra("Lose", countLose)
        setResult(Activity.RESULT_OK, intent)
        Check(sum)
        ResultCount()
        binding.myGame = myGame

    }

    private fun Check(sum: Int) {
        val ranBtn = Random.nextInt(1, 3)

        if (ranBtn == 1) {
            binding.apply {
                binding.btnSum1.text = sum.toString()
                binding.btnSum2.text = (sum + 1).toString()
                binding.btnSum3.text = (sum + 2).toString()
            }
        }
        if (ranBtn == 2) {
            binding.apply {
                binding.btnSum1.text = (sum - 1).toString()
                binding.btnSum2.text = sum.toString()
                binding.btnSum3.text = (sum + 1).toString()
            }
        }
        if (ranBtn == 3) {
            binding.apply {
                binding.btnSum1.text = (sum - 2).toString()
                binding.btnSum2.text = (sum - 1).toString()
                binding.btnSum3.text = sum.toString()
            }
        } else {
            binding.apply {
                binding.btnSum1.setOnClickListener {
                    if (binding.btnSum1.text == sum.toString()) {
                        countWin += 1
                        StartGame()
                    } else {
                        countLose += 1
                        StartGame()
                    }
                }
                binding.btnSum2.setOnClickListener {
                    if (binding.btnSum2.text == sum.toString()) {
                        countWin += 1
                        StartGame()
                    } else {
                        countLose += 1
                        StartGame()
                    }
                }
                binding.btnSum3.setOnClickListener {
                    if (binding.btnSum3.text == sum.toString()) {
                        countWin += 1
                        StartGame()
                    } else {
                        countLose += 1
                        StartGame()
                    }
                }
            }
        }
    }

    private fun ResultCount(): Pair<TextView, TextView> {
        binding.apply {
            binding.textCountWin.text = "Win: "+countWin.toString()
            binding.textCountLose.text = "Lose: "+countLose.toString()
            return Pair(binding.textCountWin, binding.textCountLose)
        }
    }

    private fun Game(): Int {

        binding.apply {
            binding.textNum1.text = num1.toString()
            binding.textNum2.text = num2.toString()
        }
        return num1 + num2
    }
}